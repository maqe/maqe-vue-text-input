export const PRESET = {
	ENGLISH_ALPHABET_ONLY: /^[a-zA-Z]*$/,
	ENGLISH_AND_NUMBER_ONLY: /^[a-zA-Z0-9]*$/,
	ENGLISH_AND_NUMBER_AND_SPECIAL_CHARACTERS_ONLY: /^[a-zA-Z0-9[\]"'!“#฿$%&‘’(){}|*+,-.:;<=>?@^_/\\`~” ]*$/,
	NUMBER_ONLY: /^[0-9]*$/,
	REMOVE_FIRST_ZERO: /^(0+)/,
	PHONE_EXTENSION: /^[0-9|,]*$/,
	SEPARATED_COMMA: /\B(?=(\d{3})+(?!\d))/g,
	PERCENTAGE: /^[0-9]$|^[1-9][0-9]$|^(100)$/
};

export const KeyCode = {
	BACKSPACE: 8
};