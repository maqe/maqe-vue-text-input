import { PRESET, KeyCode } from "./enum";

export const strToRegex = (str) => {
	const match = /^\/(.*)\/([a-z]*)$/.exec(str);
	return new RegExp(match[1], match[2]);
};

export const onKeyPress = (e) => (pattern) => {
	if (!pattern) {
		return true;
	}

	// In case cannot find by name then use new custom instead.
	const regex = PRESET[pattern] || strToRegex(pattern);

	const keyCode = e.which ? e.which : e.keyCode;
	const keyChar = String.fromCharCode(keyCode);

	if (keyCode === KeyCode.BACKSPACE || regex.test(keyChar)) {
		return true;
	}

	e.preventDefault();
	return false;
};